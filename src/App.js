import React, {Component} from 'react';
import './App.css';
import {Link, Route} from "react-router-dom";


const WebDesign = () => <div>
    <img src="https://images.unsplash.com/photo-1559028012-481c04fa702d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60" alt=""/>
    <p>Web design Lorem ipsum dolor sit amet, consectetur adipisicing elit. A architecto autem, dicta dolore enim eveniet ex expedita illo iusto, labore magnam nemo quaerat quos recusandae reiciendis! Asperiores impedit mollitia soluta!</p>
    </div>;

const GraphicDesign = () => <p>Graphic design Lorem ipsum dolor sit amet, consectetur adipisicing elit. A architecto autem, dicta dolore enim eveniet ex expedita illo iusto, labore magnam nemo quaerat quos recusandae reiciendis! Asperiores impedit mollitia soluta!</p>
const Wordpress = () => <p>Wordpress Lorem ipsum dolor sit amet, consectetur adipisicing elit. A architecto autem, dicta dolore enim eveniet ex expedita illo iusto, labore magnam nemo quaerat quos recusandae reiciendis! Asperiores impedit mollitia soluta!</p>
const Home = () => <h4>Hello! This is Home page iA architecto autem, dicta dolore enim eveniet ex expedita illo iusto, labore magnam nemo quaerat quos recusandae reiciendis! Asperiores impedit mollitia soluta!</h4>

class App extends Component {


    render() {


        return (
            <>
                <h2>React route</h2>
                <p>We need to download</p>
                <Link to={'/'}> Home </Link>
                <Link to={'/web-design'}>Web Design </Link>
                <Link to={'/graphic-design'}>Graphic Design</Link>
                <Link to={'/wordpress'}>Wordpress</Link>

                <Route path={'/'} component={Home} exact/>
                <Route path={'/web-design'} component={WebDesign}/>
                <Route path={'/graphic-design'} component={GraphicDesign}/>
                <Route path={'/wordpress'} component={Wordpress}/>
            </>
        );
    }

}

export default App;