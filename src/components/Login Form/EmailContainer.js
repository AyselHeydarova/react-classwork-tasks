import React, {useState} from 'react';
import Input from "./Input";
import SubmitBtn from "./SubmitBtn";
import Message from "./Message";


const EmailContainer = props => {
    const [state, setState] = useState({
        isCorrectEmail: false,
        isCorrectPassword: false
    });
    const validateEmail = (e) => {
        const allowedEmailServices = props.allowedEmailServices;
        const usersEmailServiceAddress = e.target.value.split('@')[1];
        setState(prevState => ({
            ...prevState,
            isCorrectEmail: allowedEmailServices.some((e)=> e === usersEmailServiceAddress )}
        ))

    };
    const validatePassword = (e) => {
        const password = event.target.value;

        if (password.length <=8 ) {
            setState({isValidationPassed: false})
        }

    };

    const submitFormHandler = () => {
            submitBtn.current = enabled
    };


    const successMessage = <Message text={'Hello!YOu entered an empty email client'}/>;

    return (
        <form onSubmit={submitFormHandler}>
            <Input type={'email'} validation={validateEmail}/>
            <Input type={'password'} validation={validatePassword}/>
            <input disabled={(state.isCorrectPass && state.isCorrectEmail)} ref={submitBtn} type={'submit'} value={'Log in'}/>
        </form>
    );
};

export default EmailContainer;