import React from 'react';

const TabHeader = props => {
    // const tabHeader = props.tabTitle.map(tab => <h3>{tab}</h3>)
    return (
        <span className={'tabs-header'} onClick={props.click}>
           {props.text};
        </span>
    );
};

export default TabHeader;