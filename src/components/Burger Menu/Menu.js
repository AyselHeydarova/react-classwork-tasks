import React from 'react';

const Menu = props => {
    const map = props.items.map((strItem, index) => <div key={index} className={'menu-item'}>{strItem}</div>);
    return (
        <div className={`menu ${props.className}`}>

            {map}

        </div>
    );
};

export default Menu;