import React, {Component} from 'react';
import BurgerButton from "./BurgerButton";
import Menu from "./Menu";
import PropTypes from 'prop-types';
import './Burger.scss';

class MenuSystem extends Component {
    state = {
        show: false,
    };

    MenuHandler = () => {
        console.log("clicked Burger");
        this.setState({show: !this.state.show})
    };


    render() {

        return (
            <React.Fragment>
                <BurgerButton clickHandler={this.MenuHandler}
                />
                {
                    this.state.show ?
                        <Menu className={this.state.show}
                              items={this.props.menuItems}/>
                        : null
                }
            </React.Fragment>
        );
    }
}

MenuSystem.defaultProps = {
    menuItems: ['menu1', 'menu2', 'menu3', 'menu4', 'menu5']
};

MenuSystem.propTypes= {
    menuItems: PropTypes.arrayOf(PropTypes.string)
};


export default MenuSystem;