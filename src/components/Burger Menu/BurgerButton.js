import React from 'react';

const BurgerButton = props => {
    return (
        <button onClick={props.clickHandler} className={'burger-btn'}>
            <div className={'line'}></div>
            <div className={'line'}></div>
            <div className={'line'}></div>
        </button>
    );
};

export default BurgerButton;