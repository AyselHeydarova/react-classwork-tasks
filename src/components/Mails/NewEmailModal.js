import React, {Component} from 'react';
import  './NewEmailModal.scss';

class NewEmailModal  extends Component {

    closeModal = (e) => {
        if(e.target.classList.contains('modal-bg')){
            this.props.closeHandler();
        }
    };


     render() {
         return (
             <div className={'modal-bg'} onClick={this.closeModal}>
                 <form className={'email-modal'} onSubmit={this.props.submitHandler}>
                     <input placeholder={'Subject'} id={'subject'}/>
                     <input placeholder={'From'} id={'mailFrom'}/>
                     <input placeholder={'To'} id={'mailTo'}/>
                     <textarea id={'text'}  cols="30" rows="10"/>


                     <input type={'submit'}/>

                 </form>
             </div>
         );
     };
}



export default NewEmailModal;