import React, {useState} from 'react';
import Preloader from "../Preloader/Preloader";
import Email from "./Email";
import Button from "./Button";

//
// class MailList extends Component {
//     state = {
//         emails: {}
//     };
//
//     componentDidMount() {
//         fetch('http://mail.danit.com.ua/email').then(r=> r.json())
//             .then((data) => {
//             this.setState({emails:{...data}});
//             console.log(this.state)
//         })
//     }
//
//     render() {
//         let emails = this.state.emails.inbox;
//         console.log('render was called - ', emails);
//
//         console.log('map was called - ', emails);
//
//         return (
//             <>
//                 {
//                     emails ? console.log("wait a little bit")
//                         : <Email email={this.emails}/>
//                 }
//
//                 {/*<Email email={emails}/>*/}
//                 {/*/!*{*!/*/}
//                 {/*    emails ?*/}
//                 {/*        emails*/}
//                 {/*    : <Preloader/>*/}
//                 {/*}*/}
//
//             </>
//         );
//     }
// }
//


const MailList = props => {
    const emails = props.emails.map((email,ind)=> <Email key={ind} self={email}/>);
    const [toggleFolderCurrentVal, setToggleFolder] = useState({toggleFolder:false});

    const toggleFolderHandler = () => {
        setToggleFolder(!toggleFolderCurrentVal);
    };

    return(
        <>
        <div className={'emails'}>
            <h1 className={'emails-folder-name'} onClick={toggleFolderHandler}>{props.folderTitle}</h1>
            <div className="emails-container" hidden={toggleFolderCurrentVal}>
                {emails}
            </div>

        </div>


        </>
    )
}
export default MailList;